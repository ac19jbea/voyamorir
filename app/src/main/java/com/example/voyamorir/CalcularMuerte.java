package com.example.voyamorir;

import android.widget.RadioButton;

import java.io.Serializable;

public class CalcularMuerte implements Serializable {
    private String nombre;
    private String fecha;
    private String lugar;
    private String vicio;
    private String sexo;

    public CalcularMuerte(String nombre, String fecha, String lugar, String vicio,String sexo) {
        this.nombre = nombre;
        this.fecha = fecha;
        this.lugar = lugar;
        this.vicio = vicio;
        this.sexo = sexo;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getVicio() {
        return vicio;
    }

    public void setVicio(String vicio) {
        this.vicio = vicio;
    }
}





