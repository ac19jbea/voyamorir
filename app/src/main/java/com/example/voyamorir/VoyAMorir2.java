package com.example.voyamorir;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Calendar;

public class VoyAMorir2 extends AppCompatActivity implements View.OnClickListener {
    Button bfecha;
    EditText fecha,e1,e2,e3;
    private int dia,mes,ano;
    private RadioButton r1,r2,r3;
    private Spinner spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voy_a_morir2);

        bfecha = (Button)findViewById(R.id.bfecha);
        fecha = (EditText)findViewById(R.id.fecha);
        bfecha.setOnClickListener(this);

        e1=(EditText)findViewById(R.id.nombre);
        e2=(EditText)findViewById(R.id.fecha);
        e3=(EditText)findViewById(R.id.lugar);

        r1 = (RadioButton) findViewById(R.id.drogas);
        r2 = (RadioButton) findViewById(R.id.alcohol);
        r3 = (RadioButton) findViewById(R.id.sexo);

        spinner = (Spinner) findViewById(R.id.spinner);
    }

    @Override
    public void onClick(View v) {
        final Calendar c = Calendar.getInstance();
        dia=c.get(Calendar.DAY_OF_MONTH);
        mes=c.get(Calendar.MONTH);
        ano=c.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                fecha.setText(dayOfMonth+"/"+(month+1)+"/"+year);
            }
        }
        ,dia,mes,ano);
        datePickerDialog.show();
    }

    public void pasaActivity(View view) {
        if(validar()){
        Intent i = new Intent(this, VoyAMorir3.class);

        String nombre = e1.getText().toString();
        String fecha = e2.getText().toString();;
        String lugar = e3.getText().toString();;
        String radio = "";
        String sexo = spinner.getSelectedItem().toString();
        if(r1.isChecked()==true) {
            radio = r1.getText().toString();
        }
        if(r2.isChecked()==true) {
            radio = r2.getText().toString();
        }
        if(r3.isChecked()==true) {
            radio = r3.getText().toString();
        }

        CalcularMuerte muerte = new CalcularMuerte(nombre,fecha,lugar,radio,sexo);

        i.putExtra("object",muerte);
        startActivity(i);
    }}
    public boolean validar(){
        boolean retorno = true;

        String campo1 = e1.getText().toString();
        String campo2 = e2.getText().toString();
        String campo3 = e3.getText().toString();

        if(campo1.isEmpty()){
            e1.setError("Este campo no puede quedar vacio");
            retorno = false;
        }
        if(campo2.isEmpty()){
            e2.setError("Este campo no puede quedar vacio");
            retorno = false;
        }
        if(campo3.isEmpty()){
            e3.setError("Este campo no puede quedar vacio");
            retorno = false;
        }
        return retorno;
    }
}
