package com.example.voyamorir;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.TextView;



public class VoyAMorir3 extends AppCompatActivity {

    String dondona,ano,historia,nativo;
    MediaPlayer reproductor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voy_a_morir3);
        reproductor =MediaPlayer.create(this,R.raw.musica1);
        reproductor.setLooping(true);
        reproductor.start();

        TextView t1 =(TextView) findViewById(R.id.textView3);
        TextView t2 =(TextView) findViewById(R.id.textView4);

        Intent i = getIntent();
        Bundle b = i.getExtras();
        CalcularMuerte muerte = (CalcularMuerte) b.getSerializable("object");

        if(muerte.getSexo().equals(getString(R.string.Hombre))) {
            dondona=getString(R.string.Don);
            nativo=getString(R.string.nativo);
        }else{
            dondona=getString(R.string.Dona);
            nativo=getString(R.string.nativa);
        }
        if(muerte.getVicio().equals(getString(R.string.Sexo))){
            ano = getString(R.string.ano1);
            historia = getString(R.string.causa1);
        }else if(muerte.getVicio().equals(getString(R.string.Drogas))){
            ano = getString(R.string.ano2);
            historia = getString(R.string.causa2);
        }else{
            ano = getString(R.string.ano3);
            historia = getString(R.string.causa3);
        }

        t1.setText(dondona + " "+ muerte.getNombre());
        t2.setText("\n"+ getString(R.string.Dejo)+ " " + ano + "\n" + nativo +" "+ muerte.getLugar()+"\n"+ historia);


    }
}